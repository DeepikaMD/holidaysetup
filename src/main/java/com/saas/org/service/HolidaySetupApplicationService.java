package com.saas.org.service;

import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.multipart.MultipartFile;

@Component
public interface HolidaySetupApplicationService {

	public String saveCSV(MultipartFile file,Model model);
}
