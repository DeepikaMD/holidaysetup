/*
 * package com.saas.org.entity;
 * 
 * import javax.persistence.Column; import javax.persistence.Entity; import
 * javax.persistence.FetchType; import javax.persistence.GeneratedValue; import
 * javax.persistence.GenerationType; import javax.persistence.Id; import
 * javax.persistence.JoinColumn; import javax.persistence.ManyToOne; import
 * javax.persistence.Table;
 * 
 * @Entity
 * 
 * @Table(name="COUNTRY") public class Country {
 * 
 * @Id
 * 
 * @GeneratedValue(strategy = GenerationType.AUTO) public int locId;
 * 
 * @Column(name="country_name") public String countryName;
 * 
 * @ManyToOne(fetch = FetchType.LAZY, optional = false)
 * 
 * @JoinColumn(name = "locId", nullable = false) private HolidaySetupEntity
 * locID;
 * 
 * 
 * public int getLocId() { return locId; }
 * 
 * public void setLocId(int locId) { this.locId = locId; }
 * 
 * public String getCountryName() { return countryName; }
 * 
 * public void setCountryName(String countryName) { this.countryName =
 * countryName; }
 * 
 * 
 * }
 */