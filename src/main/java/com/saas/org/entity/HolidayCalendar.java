package com.saas.org.entity;

import javax.persistence.Entity;

import com.opencsv.bean.CsvBindByName;

public class HolidayCalendar {
	
	@CsvBindByName
	public String date;
	
	@CsvBindByName
	public String day;
	
	@CsvBindByName
	public String name;
	
	@CsvBindByName
	public String type;

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public HolidayCalendar(String date, String day, String name, String type) {
		this.date = date;
		this.day = day;
		this.name = name;
		this.type = type;
	}
	
}
