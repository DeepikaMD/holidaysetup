package com.saas.org;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HolidaySetupApplication {

	public static void main(String[] args) {
		SpringApplication.run(HolidaySetupApplication.class, args);
	}

}
