package com.saas.org.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.saas.org.service.HolidaySetupApplicationService;

@RestController
@RequestMapping(value="/holiday")
public class HolidaySetupApplicationController {
	
	@Autowired
	HolidaySetupApplicationService holidaySetupApp;

	 @PostMapping("/upload-csv-file")
	 public String uploadCSVFile(@RequestParam("file")MultipartFile file,Model model) {
		 holidaySetupApp.saveCSV(file,model);
		 
		 
		return "file-upload-status";
		 
	 }
	 
	
}
