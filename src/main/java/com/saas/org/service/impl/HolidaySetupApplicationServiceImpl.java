package com.saas.org.service.impl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.multipart.MultipartFile;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.saas.org.entity.HolidayCalendar;
import com.saas.org.repo.HolidaySetupApplicationRepo;
import com.saas.org.service.HolidaySetupApplicationService;

@Service
public class HolidaySetupApplicationServiceImpl implements HolidaySetupApplicationService {
	
	/*
	 * @Autowired HolidaySetupApplicationRepo repo;
	 */

	public String saveCSV(MultipartFile file, Model model) {
		if(file.isEmpty()) {
			model.addAttribute("message", "Please select a CSV file to upload.");
	         model.addAttribute("status", false);
		}else {
			try(Reader reader = new BufferedReader(new InputStreamReader(file.getInputStream()))){
				   CsvToBean<HolidayCalendar> csvToBean = new CsvToBeanBuilder(reader).
						   withType(HolidayCalendar.class).withSkipLines(1).withIgnoreLeadingWhiteSpace(true).build();
				   System.out.println(csvToBean);
				   List<HolidayCalendar> holidays = csvToBean.parse();
				   System.out.println("holidays"+holidays);
				   for(HolidayCalendar holiday : holidays){
						System.out.println(holiday);
					}
				   
				   model.addAttribute("holidays", holidays);
	                model.addAttribute("status", true);
	         }catch(Exception ex) {
				 model.addAttribute("message", "An error occurred while processing the CSV file.");
	                model.addAttribute("status", false);
			}
		}
		return "file-uploaded";
	}
	
	
}
